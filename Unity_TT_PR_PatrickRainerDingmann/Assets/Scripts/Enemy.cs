﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public bool isEnemyAlive;
    public int enemyHealth;
    public int enemyDamage;

    public void EnemyHealthCheck()
    {
        if(enemyHealth == 0)
        {
            isEnemyAlive = false;
        }
        else if(enemyHealth < 0)
        {
            Debug.Log("EnemyHealth-Error: The Health shouldn't be below zero");
        }
    }
    // Update is called once per frame
    void Update()
    {
       
    }
}
