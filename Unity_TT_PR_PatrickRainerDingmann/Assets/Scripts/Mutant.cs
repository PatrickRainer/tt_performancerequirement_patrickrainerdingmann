﻿using UnityEngine;
using System.Collections;

public class Mutant : Enemy
{

    private string EnemyNameTag = "Mutant";

    private void Start()
    {
        isEnemyAlive = true;
        enemyHealth = 120;
        enemyDamage = 60;
    }


    // Update is called once per frame
    void Update()
    {

        EnemyHealthCheck();
    }
}
