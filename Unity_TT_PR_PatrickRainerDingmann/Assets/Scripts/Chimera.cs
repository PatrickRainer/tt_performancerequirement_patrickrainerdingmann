﻿using UnityEngine;
using System.Collections;

public class Chimera : Enemy
{

    private string EnemyNameTag = "Chimera";

    private void Start()
    {
        isEnemyAlive = true;
        enemyHealth = 500;
        enemyDamage = 210;
    }


    // Update is called once per frame
    void Update()
    {

        EnemyHealthCheck();
    }
}
