﻿using UnityEngine;
using System.Collections;

public class Zombie : Enemy
{
    private string EnemyNameTag = "Zombie";

    private void Start()
    {
        isEnemyAlive = true;
        enemyHealth = 50;
        enemyDamage = 20;
    }


    // Update is called once per frame
    void Update()
    {
       
        EnemyHealthCheck();
    }
}
